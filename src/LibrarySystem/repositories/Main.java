package LibrarySystem.repositories;

import LibrarySystem.data.interfaces.IDB;
import LibrarySystem.data.PostgresDB;
import LibrarySystem.entities.borrowBook.BorrowBookController;
import LibrarySystem.entities.borrowBook.BorrowBookRepository;
import LibrarySystem.entities.borrowBook.BorrowBookRepositoryInter;
import LibrarySystem.entities.user.userMethods.UserController;
import LibrarySystem.entities.user.userMethods.UserRepository;
import LibrarySystem.entities.user.userMethods.UserRepositoryInter;
import LibrarySystem.entities.book.BookMethods.BookController;
import LibrarySystem.entities.book.BookMethods.BookRepository;
import LibrarySystem.entities.book.BookMethods.BookRepositoryInter;

import java.sql.*;

public class Main {

    public static void main(String[] args) {
//        String connectionUrl = "jdbc:postgresql://localhost:5432/LibrarySystem";
//        Connection con = null;
//        ResultSet rs = null;
//        Statement stmt = null;
//        try {
//            // Here we load the driver’s class file into memory at the runtime
//            Class.forName("org.postgresql.Driver");
//
//            // Establish the connection
//            con = DriverManager.getConnection(connectionUrl, "postgres", "asp20042004");
//
//            // The object of statement is responsible to execute queries with the database
//            stmt = con.createStatement();
//
//            // The executeQuery() method of Statement interface is used to execute queries
//            // to the database. This method returns the object of ResultSet that can be
//            // used to get all the records of a table that matches the sql statement
//            rs = stmt.executeQuery("select * from users");
//
//            while (rs.next()) // Processing the result
//                System.out.println(rs.getInt("id") + "  "
//                        + rs.getString("name") + "  " + rs.getString("group"));
//        }
//        catch (Exception e) {
//            System.out.println("Exception occurred!");
//        } finally {
//
//            try { // Close connection - clean up the system resources
//                con.close();
//            } catch (Exception e) {
//                System.out.println("Exception occurred!");
//            }
//        }
//
//        System.out.println("Finished!");

        // Here you specify which DB and UserRepository to use
        // And changing DB should not affect to whole code
        IDB db = new PostgresDB();

        UserRepositoryInter repo1 = new UserRepository(db);
        UserController controller1 = new UserController(repo1);

        BookRepositoryInter repo2 = new BookRepository(db);
        BookController controller2 = new BookController(repo2);

        BorrowBookRepositoryInter repo3 = new BorrowBookRepository(db);
        BorrowBookController controller3 = new BorrowBookController(repo3);

        MyApplication app = new MyApplication(controller1, controller2, controller3);

        app.start();
    }
}
